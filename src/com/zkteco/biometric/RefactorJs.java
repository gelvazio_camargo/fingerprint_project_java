package com.zkteco.biometric;

import java.io.FileNotFoundException;
import java.io.FileReader;
import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

/**
 *
 * @author gelvazio
 */
public class RefactorJs {
    
    public static void main(String[] args) {
        try {
            int n = 5;
            multiplicarNumero(n);            
        } catch (Exception e) {
        }
    }
    
    public static void multiplicarNumero(int numero) throws NoSuchMethodException, FileNotFoundException, ScriptException { 
        //NoShuchMethod (Método não encontrado), 
        //FileNotFound (Arquivo não encontrado), 
        //ScriptException (Como o nome já diz, tem erro no seu script).
        
        // Permite escolher linguagens diferentes da tradicional Java
        ScriptEngineManager factory = new ScriptEngineManager(); 
        
        //Escolhemos a linguagem que será utilizada, nesse caso é o JavaScript
        ScriptEngine engine = factory.getEngineByName("JavaScript"); 
        Invocable invocable = (Invocable) engine;

        //escolhemos onde o arquivo está, pasta+arquivo.extensao
        //engine.eval(new FileReader("C:\\Imperium\\script.js")); 
        
        //String pathScript = "C:\\Fontes\\fingerprint_project\\app.js";
        String pathScript = "C:\\Fontes\\fingerprint_project\\src\\com\\zkteco\\biometric\\app.js";
        engine.eval(new FileReader(pathScript)); 
        
        //Aqui chamamos o a Function do nosso Script ("multiplicar") e passamos a variável "número" que declaramos no método.
        Double resultado = (Double) invocable.invokeFunction("multiplicar", numero); 
        
        //Aqui a gente mostra o resultado
        System.out.println("O resultado da sua função é: " + resultado); 
    }
}
