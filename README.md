<h1 align="center">
    <img alt="ZKteco" title="#ZKteco" src=".github/logo_leitor_1.png" width="250px" />
</h1>

<h4 align="center">
  ☕ Code and coffee
</h4>
<p align="center">
  <img alt="GitHub language count" src="https://img.shields.io/github/issues/Gelvazio/fingerprint_project_java.svg">

  <img alt="Repository size" src="https://img.shields.io/github/repo-size/Gelvazio/fingerprint_project_java.svg">
  
  <a href="https://github.com/Gelvazio/fingerprint_project_java/commits/master">
    <img alt="GitHub last commit" src="https://img.shields.io/github/last-commit/Gelvazio/fingerprint_project_java.svg">
  </a>

  <a href="https://github.com/Gelvazio/fingerprint_project_java/issues">
    <img alt="Repository issues" src="https://img.shields.io/github/issues/Gelvazio/fingerprint_project_java.svg">
  </a>

  <img alt="License" src="https://img.shields.io/badge/license-MIT-brightgreen">
</p>

<p align="center">
  <a href="#tecnologias">Tecnologias</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#-projeto">Projeto</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;  
  <a href="#-como-contribuir">Como contribuir</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#memo-licença">Licença</a>
  <a href="#biblioteca">Biblioteca</a>
</p>

<br>

<p align="center">
  <img alt="Frontend" src=".github/logo_leitor_2.png" width="100%">
</p>

## Biblioteca
Numero de serie do aplicativo leitor biometrico:SLK20r
Acesse o site [aqui](https://www.zkteco.com.br/leitores-biometricos/).

Download da biblioteca [aqui](https://www.zkteco.com.br/zkfinger-sdk-5-3/)

## Tecnologias

Esse projeto foi desenvolvido com as seguintes tecnologias:

- [Java](https://www.java.com/pt_BR/)

## 💻 Projeto

O projeto é uma adaptação da bibioteca em java com simulador do leitor ZKTeco.

## 🤔 Como contribuir

- Faça um fork desse repositório;
- Cria uma branch com a sua feature: `git checkout -b minha-feature`;
- Faça commit das suas alterações: `git commit -m 'feat: Minha nova feature'`;
- Faça push para a sua branch: `git push origin minha-feature`.

Depois que o merge da sua pull request for feito, você pode deletar a sua branch.

## :memo: Licença

Esse projeto está sob a licença MIT. Veja o arquivo [LICENSE](LICENSE.md) para mais detalhes.
